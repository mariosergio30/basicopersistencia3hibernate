package curso.dao;

import jakarta.persistence.*;

import curso.model.AlunoEntity;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;

import java.sql.SQLException;
import java.util.List;

public class AlunoDao implements InterfaceCrudDAO<AlunoEntity> {

	String QUERY_ALL = "query.all";

	private EntityManager entityManager;


	public AlunoDao(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}



	@Override
	public boolean create(AlunoEntity obj) throws SQLException {

		try {
			entityManager.getTransaction().begin();
			entityManager.persist(obj);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}



	@Override
	public List<AlunoEntity> read() {

		try {
			Query query = entityManager.createNamedQuery(QUERY_ALL);
			List<AlunoEntity> lista = query.getResultList();
			return lista;

		} catch (NoResultException e) {
			return null;
		}
		
	}


	@Override
	public AlunoEntity readOne(String id) {

		try {
      		AlunoEntity aluno = entityManager.find(AlunoEntity.class,id);
			return aluno;

		} catch (NoResultException e) {
			return null;
		}
	}



	@Override
	public boolean update(AlunoEntity obj) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(obj);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	@Override
	public boolean delete(AlunoEntity obj) {
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(obj);
			entityManager.getTransaction().commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}



	public List<AlunoEntity> executeCriteriaQuery(CriteriaQuery<AlunoEntity> cquery) {

		try {
			Query query = entityManager.createQuery(cquery);
			List<AlunoEntity> lista = query.getResultList();
			return lista;

		} catch (NoResultException e) {
			return null;
		}
	}

	public CriteriaQuery<AlunoEntity> getCriteriaQuery() {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<AlunoEntity> cquery =	 cb.createQuery(AlunoEntity.class);
		return cquery;
	}



}
