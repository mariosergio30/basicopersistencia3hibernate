package curso.dao;

import java.sql.SQLException;
import java.util.List;

//Toda Classe que DAO deverá implementar esta interfaceCRUD
public interface InterfaceCrudDAO<T> {
	
	public boolean create(T obj) throws SQLException;  // (INSERT/CREATE)

	public List<T> read(); 		// (RECUPERA CONJUNTO)

	public T readOne(String chave);  //  (RECUPERA UM ELEMENTO)

	public boolean update(T obj);  // (UPDATE)
	
	public boolean delete(T obj);  	// (DELETE)


	
	
}
