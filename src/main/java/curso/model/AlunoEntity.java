package curso.model;



/* se o nome da tabela fosse exatamente o nome da classe (Disciplina),
  o uso de Anottation seria opcional 
  obs: o mesmo vale para @Column*/

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table(name = "ALUNO")
@Entity
public class AlunoEntity {

	@Id
	@Column(nullable=false, length = 8, columnDefinition = "CHAR")
	private String matricula;

	@Column(name = "cpf", nullable = false, unique = true, columnDefinition = "CHAR")
    private String cpf;

    private String nome;

	@Column(nullable=false, columnDefinition = "CHAR")
	private String sexo;

	Integer idade;


	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}
}