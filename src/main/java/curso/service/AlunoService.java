package curso.service;

import curso.dao.AlunoDao;
import curso.model.AlunoEntity;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;


public class AlunoService {

	private AlunoDao alunoDao;

	public AlunoService(EntityManager entityManager) {
		super();

		/* objeto gerenciado pelo HIBERNATE */
    	alunoDao = new AlunoDao(entityManager);
	}


	// matricula de um novo aluno 
    public boolean matricular(AlunoEntity a) {

		try {
			if (alunoDao.create(a)) {
				return true;
			}
		}
		catch (Exception e) {
			System.out.println("Erro ao matricular " + e.getMessage());
		}


    	return false;
    }
	
		
	
	// saida de um aluno da escola 
	public boolean removeAluno(AlunoEntity a) {

	  	if (alunoDao.delete(a))
			return true;

		 return false;

	}


	public AlunoEntity consultaAlunoPorMatricula(String matricula) {

		return alunoDao.readOne(matricula);
	}


	/* retorna a lista completa de Alunos
	 matriculados */
	public List<AlunoEntity> consultaAlunos() {
		return alunoDao.read();
	}

	

}
