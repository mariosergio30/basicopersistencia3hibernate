package curso;



import curso.model.AlunoEntity;
import curso.service.AlunoService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import java.util.List;

public class Application {

    private static AlunoService alunosService;

    private static EntityManager entityManager;

    public static void main(String[] args) {

        // ConexaoJDBC con = conectaDB(); //  SUBSTITUIDO PELO EntityManager + persistence.xml do HIBERNATE

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("dbEscola");
        entityManager = factory.createEntityManager();

        alunosService = new AlunoService(entityManager);

         consultaDisciplinas();

        inserirAlunos();

        consultaTodosAlunos();

        consultaUmAluno("00000005");


        /* DO DO
        updateAluno();
        inserirDisciplina();
		inserirProfessor();

        consultaProfessoresByDisciplina();

        consultaOneAula();   // retorna também nome do professor e da disciplina

        consultaAulas();   // retorna também nome do professor e da disciplina
        */

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< FIM >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");


    }


    /**
    public static ConexaoJDBC conectaDB() {

        //  SUBSTITUIDO PELO persistence.xml do HIBERNATE

    }
    **/

    public static boolean inserirAlunos() {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de INSERT >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;

        AlunoEntity aluno1 = new AlunoEntity();
        aluno1.setNome("Aluno Exemplo");
        aluno1.setIdade(21);
        aluno1.setCpf("000.001.003.13");
        aluno1.setMatricula("78822667");
        aluno1.setSexo("M");

        if (alunosService.matricular(aluno1)) {
            System.out.println("OK");
            return true;
        }

        return false;

    }


    public static void consultaDisciplinas() {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL Disciplinas  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("Consultando primeira e ultima disciplina...");

        System.out.println("TO DO");

    }




    public static void consultaTodosAlunos() {


        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL (com retorno Todos os Registros)  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;


        List<AlunoEntity> listAlunos = alunosService.consultaAlunos();

        for(AlunoEntity a : listAlunos){
            System.out.println("Nome " + a.getNome() + " - matricula " + a.getMatricula());

        }


    }


    public static void consultaUmAluno(String matricula) {

        System.out.println("");
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<< execução de CONSULTA SQL (Um Aluno)  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>") ;

        AlunoEntity aluno = alunosService.consultaAlunoPorMatricula(matricula);

        if (aluno != null) {

            System.out.println("Nome: " + aluno.getNome() + " CPF: " + aluno.getCpf());
        }

    }



}
